LoRa - Long Range and Low Power
===============================

LoRa Radio
----------

There are different radio modules available, produced by Semtech who have the LoRa radio patent.

* Semtech SX1275, SX1277 - single channel modules
* Semtech SX1301 - 8 channel module (used in gateways)

A data frame is sent out a LoRa radio as a short spread spectrum chirp:

.. image:: _static/images/spectrum-lora.png
   :width: 650px
   :alt: LoRaWAN Spectrum

Data Rates
----------

Our channel plans follow the USA channel plans except our uplink channels start higher at 915MHz-928MHz (USA is
902-928 MHz).

The channel plan for Australia is:

======== =========   =============  =============  =========
CHANNEL# DIRECTION   FREQUENCY MHZ  BANDWIDTH KHZ  DATA RATE
======== =========   =============  =============  =========
8  	     UP          916.8          125            DR0 – DR3
9        UP          917.0          125            DR0 – DR3
10       UP          917.2          125            DR0 – DR3
11       UP          917.4          125            DR0 – DR3
12       UP          917.6          125            DR0 – DR3
13       UP          917.7          125            DR0 – DR3
14       UP          918.0          125            DR0 – DR3
15       UP          918.2          125            DR0 – DR3
65       UP          917.5          500            DR4
0        DOWN        923.3          500            DR8 – DR13
1        DOWN        923.9          500            DR8 – DR13
2        DOWN        924.5          500            DR8 – DR13
3        DOWN        925.1          500            DR8 – DR13
4        DOWN        925.7          500            DR8 – DR13
5        DOWN        926.3          500            DR8 – DR13
6        DOWN        926.9          500            DR8 – DR13
7        DOWN        927.5          500            DR8 – DR13
======== =========   =============  =============  =========

LoRaWAN Network Stack
---------------------

Here are the building blocks of LoRaWAN Network:

.. image:: _static/images/lorawan-network.png
   :width: 650px
   :alt: LoRaWAN Network

In our workshop you can see the gateway that has been set up, and the "Network Server" is The Things Network
infrastructure. That leaves us to set up the end nodes and the application servers. We might only need to set up the
end nodes, as TTN gives us some useful app integrations.

End devices are connected to applications running on the LoRaWAN network. Multiple applications can be transmitted
through the same gateway, as each device and application has it's own application key. This is used to generate
the network session key and application session key"

.. image:: _static/images/keys.png
   :alt: LoRaWAN Keys


Gateways
^^^^^^^^

* The gateway should be a multi-channel radio module so it can listen on a broad range of channels and data rates.
* The LoPy can act as a single channel gateway which is not technically compliant with the LoRaWAN specification
* The gateway needs an NTP connection to keep current time so encryption works and for timestamping.
* The gateway receives packets from end-nodes and runs a packet forwarder, sending the payload to a remote server
* The gateway has a network key, so all devices with an application with that network key can talk to the gateway.

Here is the multichannel gateway soon to be installed, and our Pycom single channel gateway next to it.

.. image:: _static/images/gateways.jpg
   :alt: Gateways


Internet Infrastructure
^^^^^^^^^^^^^^^^^^^^^^^

The gateway sends packets to the packet routers which then delivers it to the final application service.

Some gateway packet forwarders use MQTT for this as it is a lightweight and sometimes the gateways run on
constrained devices such as underpowered OpenWRT routers/access-points.
