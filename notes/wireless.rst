Wireless Networks
=================

The Internet
------------

* IPv4 has 32 billion IP addresses, not enough for everyone and their devices
* IPv6 has 2\ :sup:`128` or 3.4 x 10\ :sup:`38` addresses
* Do you want your IoT devices directly accessible from every Tom, Dick and Hacker on the Internet?
* The Internet was always the connecting glue between many types of disparate networks.
* Routers and gateways provide the interface to these networks

Examples of Wireless Networks
-----------------------------

.. only:: latex

   .. image:: _static/images/DataRateVsRange.png
      :height: 650px
      :width: 650px
      :alt: Data Rate Vs Range

.. only:: html

   .. image:: _static/images/DataRateVsRange.svg
      :height: 650px
      :width: 650px
      :alt: Data Rate Vs Range

WiFi
----

* 2.4GHz or 5GHz
* High throughput - 150Mbps up to 867Mbps
* WiFi good for 400m  and much less indoors due to obstacles.
* WiFi can go further at higher rates with point to point links and high gain antennas
* Inexpensive wireless chips, eg ESP8266 for $4, ESP32 for $10

Bluetooth
---------

* Designed for PAN - Personal Area Networks and low energy devices
* 2.4GHz and low power
* Usually for pairing of devices
* Recently BlueTooth Mesh specification has been announced could make it interesting for IoT networks.

3G/4G
-----

* Expensive commercial spectrum shared between many customers
* High power and high data rates
* You need a paid SIM card and LTE/3G module to access these networks
* 850/900MHz for 3G, many different frequencies for 4G, including 700MHz

ZigBee / 802.15.4
------------------

* Lower data rate than WiFi and more range
* Supplemented with mesh technology to make a network of low powered devices
* Some vendors also make 915MHz version
* Radio chips $30-50 USD
* Many different vendors and standards.

.. image:: _static/images/zigbeeprotocols.png


915MHz and other ISM Bands
--------------------------

- ISM bands 915MHz and 433MHz - RF spectrum reserved for RF Energy emitted by Industrial, Scientific, Medical devices
- Worldwide either 868MHz or 915MHz - ensure you get the right device for Australia
- 915MHz in Australia
- 915MHz has better range than 2.4GHz, also not prone to fading due to water absorption
- LoRa radios fit in here, as well as some ZigBee
- Many other manufacturers offer their own 'sub-GHz' radio "standards"
- Radio chips around $20
- Longer wavelength == longer antenna

