MicroPython - the 10 Minute Guide
=================================

Using a GPIO Pin for Digital Output
-----------------------------------

.. code-block:: python

    from machine import Pin
    import time

    led_pin = Pin('G7', mode=Pin.OUT)
    load_pin = Pin('G10', mode=Pin.OUT, pull=Pin.PULL_DOWN)

    load_pin(1)     # Shortcut to load_pin.value(1)
    while True:
        led_pin.toggle()
        time.sleep_ms(500)


The first argument of the Pin constructor can either be the 'Px' pin from the LoPy pinout or the 'Gx' pin from the
expansion board layout.

The mode can be:
* Pin.IN
* Pin.OUT
* Pin.OPEN_DRAIN - input or output in open drain mode

The pull can be:
* Pin.PULL_UP
* Pin.PULL_DOWN

Using a GPIO Pin for Digital Input
----------------------------------

.. code-block:: python

    from machine import Pin

    switch_pin = Pin('G22', mode=Pin.IN, mode=Pin.PULL_UP)

    while switch_pin.value() == 0:
        pass    # do nothing

    print('Pin value is now {}'.format(switch_pin.value()))

Digital Input Interrupts as Callbacks
-------------------------------------

The point of an interrupt is to break the main program flow when an event occurs to execute an interrupt function.
We can assign a function to be called when a digital input put rises and/or falls between zero and one.

.. code-block:: python

    from machine import Pin
    import time

    def handle_switch_turned_on(pin):
        print('Switch on pin {} was turned on!'.format(pin.id))

    switch_a = Pin('G7', mode=Pin.IN)
    switch_b = Pin('G8', mode=Pin.IN)

    # Both switches will call the same interrupt function
    switch_a.callback(Pin.IRQ_RISING, handler=handle_switch_turned_on)
    switch_b.callback(Pin.IRQ_RISING, handler=handle_switch_turned_on)

    # Main program
    while True:
        print('Nothing happening!')
        time.sleep_ms(5000)

In this example, the main program prints 'Nothing happening!' every 5 seconds until you press either switches.

Using a GPIO Pin for Analog Input
---------------------------------

GPIO pins are P13 to P20 of the LoPy can be used as ADC channels (see the pinout)

.. code-block:: python

    import machine
    import time

    adc = machine.ADC()             # create an ADC object
    apin = adc.channel(pin='P16')   # create an analog pin on P16
    bpin = adc.channel(pin='P17', bits=9)  # lower resolution!

    while True:
        val = apin()                    # read an analog value
        adc_percent = val / 4096 * 100  # default 12 bit ADC resolution
        print('ADC value is now at {0:.1f}%'.format(adc_val))   # 1 decimal point
        time.sleep_ms(1000)

Using two pins for Serial Communication
---------------------------------------

UART0 is normally assigned to the USB device but you can also re-assign it to other pins or use UART1. This could be
useful if you are talking to another serial device, eg anouther microcontroller or piece of equipment with a serial
or debug port.

.. code-block:: python

    from machine import UART

    uart = UART(1, 9600)
    uart.init(9600, bits=8, parity=None, stop=1, pins=('P3', 'P4'))

The docs say you can also specify RTS/CTS pins, but as of last week I was unable to make the RTS/CTS pins move so I
doubt this works properly. So right now there is no flow control but this may be fixed in the future.

Other Protocols
---------------

The LoPy board also has I2C, SPI, OneWire and DAC capabilities but these are beyond the scope of this workshop.

Refer to the PyCom documentation website for examples on how to use these devices: https://docs.pycom.io

Opening Files
-------------

.. code-block:: python

    with open('/flash/config.json', 'r') as f:
        lines = f.readlines()

    # file is now closed
    for line in lines:
        print('config says: {}'.format(line))

Timers
------

Here is an example of how to use pin interrupts to make a stop-watch.

.. code-block:: python


    from machine import Pin, Timer

    chrono = Timer.Chrono()

    def handle_finish_trigger(pin):
        elapsed = chrono.read()
        chrono.stop()
        chrono.reset()
        print('Finished - time {}'.format(elapsed))

    def handle_start_trigger(pin):
        chrono.start()
        print('Started!')

    start_pin = Pin('G7', Pin.IN)
    start_pin.callback(Pin.IRQ_RISING, handler=handle_start_trigger)
    finish_pin = Pin('G8', Pin.IN)
    finish_pin.callback(Pin.IRQ_RISING, handler=handle_finish_trigger)

    while True:
        pass    # endless empty loop


Differences Between Python and MicroPython
------------------------------------------

* Not all of the Python standard library is available.
* MicroPython uses garbage collection, Python uses reference counting
* Not complete implementation of functions and classes
