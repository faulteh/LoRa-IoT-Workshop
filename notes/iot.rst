Internet of Things
==================

The Internet of Things (IoT) is the inter-networking of physical devices, 
vehicles (also referred to as "connected devices" and "smart devices"), 
buildings, and other items embedded with electronics, software, sensors, 
actuators, and network connectivity which enable these objects to collect 
and exchange data. (Wikipedia)

Microcontrollers
----------------

.. image:: _static/images/arduino-uni.jpg
   :width: 600px
   :alt: Arduino

.. image:: _static/images/pizero.jpg
   :width: 600px
   :alt: Pi Zero


.. image:: _static/images/teensy.jpg
   :width: 600px
   :alt: Teensy


.. image:: _static/images/lopy.jpg
   :width: 600px
   :alt: LoPy


.. image:: _static/images/esp12.jpg
   :width: 600px
   :alt: ESP12


Available Sensors
-----------------

- Temperature and humidity
- GPS
- Proximity/distance
- Soil moisture
- Light intensity
- Water flow
- Wind
- Weight
- RFID
- Gas
- Passive Infrared
- Voltage and Current

Available Transducers
---------------------

- Relay board
- Mosfets for solid state turning on and off and PWM
 