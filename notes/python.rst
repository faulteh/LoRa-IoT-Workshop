Python - the 10 Minute Guide
============================

Python has just been voted top programming language by the IEEE Spectrum [IEEE2017]_. It is an amazingly
flexible language, easy to learn and a huge ecosystem of modules and support.  Python is used
everywhere, from web applications, data science, finance, documentation [#f1]_, native GUI apps, and mobile apps.

It's most recent incarnation is MicroPython - an interpreter for microcontrollers such as the STM32 and ESP32.

The REPL
--------

The interactive (Micro)Python interpreter is called the REPL (Read-Eval-Print-Loop). In any source code examples,
lines starting with ``>>>`` or ``...`` indicate you are typing these commands in the REPL. You can use the REPL to try
out code and syntax to get it right before you commit it to your source code.

All of the examples below up to the section on Functions and Classes are typed in the REPL.

Data Types
----------

Integers, floats
^^^^^^^^^^^^^^^^

.. code-block:: python

    >>> 64 * 3
    192
    >>> 64.01 * 2.99
    255.39990000000003
    >>> 1e8
    100000000.0

Complex and Large Numbers
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    >>> a = 1 + 2j
    >>> a = 1+2j
    >>> a
    (1+2j)
    >>> b = -1+1j
    >>> a+b
    3j
    >>> 12**433
    1929654420184169927181932593413015423444633820799982064655498812353503327509
    5407498722889956375278682482387890890524228533078626903823583124156423890248
    4418160286891119374790182156369097455245825685968255869001217169494278986439
    8181368470804314422340439653561633249309093635536874726933327296472308299026
    4560685450745454545198151190888718507209377228992080675951159430027775012897
    7969551072234962934954444280924269536990065472519247592147945178646068601826
    857736732672

Boolean
^^^^^^^

.. code-block:: python

    >>> 1 + 3 = 4
    True
    >>> 6 - 9 > 0
    False
    >>> not True
    False

Strings (and bytes)
^^^^^^^^^^^^^^^^^^^

All strings in Python [#f2]_ are Unicode strings, eg UTF-8, rather than ASCII (which are 8 bit bytes). This
makes it easy to internationalise your application and even use some emojis

.. code-block:: python

    >>> a = 'Python makes writing apps more easy.'
    >>> a.replace('more easy', 'easier')
    'Python makes writing apps easier.'
    >>> a = 'Python makes writing apps {}.'
    >>> a.format('fun')
    'Python makes writing apps fun.'
    >>> 'I can write apps %s than C++ and %d%%easier to read than %s' % ('much faster', 100, 'JavaScript')
    'I can write apps much faster than C++ and 100%easier to read than JavaScript'
    >>> 'I can write apps %s than C++ and %d%% easier to read than %s' % ('much faster', 100, 'JavaScript')
    'I can write apps much faster than C++ and 100% easier to read than JavaScript'

To turn a string into a list of bytes you need to encode it. Similarly to turn a list of bytes into a string
you need to encode it:

.. code-block:: python

    >>> a
    'Python makes writing apps great.'
    >>> a.encode('utf-8')
    b'Python makes writing apps great.'
    >>> a.encode('utf-8').decode('utf-8')
    'Python makes writing apps great.'
    >>> 'Literally encoded and decoded'.encode('utf-8').decode('utf-8')
    'Literally encoded and decoded'

Lists (and Strings)
^^^^^^^^^^^^^^^^^^^

A list is the same as an array in many other languages. Many list operations can also be done on strings.

A simple list:

.. code-block:: python

    >>> a_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    >>> another_list = ['one', 'two', 3, 4, 'five', 6, 7, 'eight','nine', 'ten']

Splitting strings into lists and joining them again:

.. code-block:: python

    >>> 'Python makes writing apps great!'.split(' ')
    ['Python', 'makes', 'writing', 'apps', 'great!']
    >>> ' '.join(['Python', 'makes', 'writing', 'apps', 'great!'])
    'Python makes writing apps great!'
    >>> a = 'Python makes writing apps great!'.split(' ')

Accessing items in a list (from zero!):

You can go forwards, backwards, slice and dice lists however you like.

.. code-block:: python

    >>> a_list = ['zero', 1, 2, 'three', 'four', 5, 'six', 7, 8, 'nine']
    >>> a_list
    ['zero', 1, 2, 'three', 'four', 5, 'six', 7, 8, 'nine']
    >>> a_list[0]
    'zero'
    >>> a_list[1]
    1
    >>> a_list[-1]
    'nine'
    >>> a_list[2:4]
    [2, 'three']
    >>> a_list[2:8:2]
    [2, 'four', 'six']
    >>> a_list[-1:-8:-2]
    ['nine', 7, 5, 'three']

Using Strings like a list:

.. code-block:: python

    >>> a
    'Python makes writing apps great.'
    >>> a[1]
    'y'
    >>> a[-1]
    '.'

Appending, Inserting and Popping from lists:

.. code-block:: python

    >>> a_list
    ['zero', 1, 2, 'three', 'four', 5, 'six', 7, 8, 'nine']
    >>> a_list.append(10)
    >>> a_list
    ['zero', 1, 2, 'three', 'four', 5, 'six', 7, 8, 'nine', 10]
    >>> a_list.pop()
    10
    >>> a_list
    ['zero', 1, 2, 'three', 'four', 5, 'six', 7, 8, 'nine']
    >>> a_list.insert(4, 3.14159)
    >>> a_list
    ['zero', 1, 2, 'three', 3.14159, 'four', 5, 'six', 7, 8, 'nine']
    >>> a_list.remove(7)    # Remove first occurrence of value 7
    >>> a_list
    ['zero', 1, 2, 'three', 3.14159, 'four', 5, 'six', 8, 'nine']
    >>> new_list = a_list + [10, 11, 12]
    >>> new_list
    ['zero', 1, 2, 'three', 3.14159, 'four', 5, 'six', 8, 'nine', 10, 11, 12]

Searching and Sorting:

.. code-block:: python

    >>> len(a_list)
    10
    >>> 'three' in a_list
    True
    >>> num_list = [457, 12, 123, 11,67]
    >>> num_list.sort()
    >>> num_list
    [11, 12, 67, 123, 457]
    >>> a_list.reverse()
    >>> a_list
    ['nine', 8, 'six', 5, 'four', 3.14159, 'three', 2, 1, 'zero']
    >>> a_list.sort()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: unorderable types: int() < str()

Tip: Once you get the hang of lists and for loops, check out `List Comprehensions`_ they are a most powerful tool

.. _`List Comprehensions`: https://docs.python.org/3/tutorial/datastructures.html#list-comprehensions

Dictionaries
^^^^^^^^^^^^

These can have multiple dimensions and be of mixed data types to build up complex data structures.

Lists have ```[ ]``` while dictionaries have ```{ }```.

.. code-block:: python

    >>> mydict = {}
    >>> mydict['name'] = 'Scott'
    >>> mydict['email'] = 'jsbragg@scriptforge.org'
    >>> mydict
    {'name': 'Scott', 'email': 'jsbragg@scriptforge.org'}
    >>> yourdict = {
    ...     'name': 'Sarah',
    ...     'email': 'sarah@example.com'
    ... }
    >>> contacts = [mydict, yourdict]
    >>> contacts
    [{'name': 'Scott', 'email': 'jsbragg@scriptforge.org'}, {'name': 'Sarah', 'email': 'sarah@example.com'}]
    >>> contacts[1]['name']
    'Sarah'
    >>> contacts[-2]['email']
    'jsbragg@scriptforge.org'

Program Structures
------------------

Unlike many other programming languages, Python does not use ```{ }``` to indicate code blocks, and lines of
code are not finished with a semi-colon;. Instead Python uses indenting to discern if you are in a program structure
such as a for loop, while loop or if-then-else structure.

**The rule of thumb: if you end your line with a colon : then you need to increase the indent of the following line(s).**

In Python world we have this thing called PEP8 which is 'best practice' writing style for Python. It recommends that
an indent is four spaces (not tabs!) and that lines of code are 79 characters or less.

When your code gets too long, you can split the line, usually at a comma, and following lines can be indented to
match close to where you are referring to on the previous line.

Here is an example:

.. code-block:: python

    long_list_of_names = ['peter', 'paul', 'scott', 'ben', 'sarah', 'alex', 'emma', 'richard', 'mary', 'craig',
                            'megan', 'michael', 'elizabeth', 'jennifer']

Each of the program structures below ends with a : which means the next line is indented.

For Loop
^^^^^^^^

.. code-block:: python

    >>> a_list
    ['nine', 8, 'six', 5, 'four', 3.14159, 'three', 2, 1, 'zero']
    >>> for item in a_list:
    ...     print(item)
    ...
    nine
    8
    six
    5
    four
    3.14159
    three
    2
    1
    zero

if-elif-else:
^^^^^^^^^^^^^

.. code-block:: python

    >>> for idx in range(15):
    ...     if idx % 2 == 0 and idx % 3 == 0:
    ...             print('fizzbuzz')
    ...     elif idx % 2 == 0:
    ...             print('fizz')
    ...     elif idx % 3 == 0:
    ...             print('buzz')
    ...     else:
    ...             pass
    ...
    fizzbuzz
    fizz
    buzz
    fizz
    fizzbuzz
    fizz
    buzz
    fizz
    fizzbuzz
    fizz


While Loop
^^^^^^^^^^

.. code-block:: python

    count = 0
    while True:
        count += 1
        if count == 100:
            count = 0
            print "reset count"

This will print 'reset count' endlessly until the program is interrupted. Normally an endless loop in a program
is a bad thing, except in microcontrollers, you *need* to have an endless loop as the main part of your program
otherwise the program will finish and the microcontroller will sit there doing **nothing**.

Functions and Classes
---------------------

Functions allow you to split your code into reusable blocks. This makes your code easier to test as you know
the inputs to the function (the parameters) and what you expect to return from the function.

.. code-block:: python

    def sum(a_list):
        sum = 0
        for item in a_list:
            sum += item
        return item

    mylist = [1, 2, 3, 4, 5, 6]
    print(sum(mylist))

This will print the value '21' to the console.

Classes allow you to store data (variables or attributes) along with functions related to that data (methods).
Classes are the blueprint of an object. An object is an instantiation of a class. You access data about an object
and methods you can do to an object using dot notation.

When you construct an object, you call the class constructor.

You use the keyword ``self`` to refer to the object. This is just like the keyword ``this`` in other languages

.. code-block:: python

    class Engine:
        rpm = 0      # set initial rpm
        state = 'off'
        fuel_level = 100.0

        # The constructor
        def __init__(self, fuel='petrol'):
            self.fuel_type = fuel

        def start(self):
            self.state = 'running'

        def run(self, seconds):
            if not self.state == 'running':
                print('Engine is not started!')
                return 0
            fuel_used = 0.000000001 * rpm * seconds       # this is a local variable
            self.fuel_level -= fuel_used
            return fuel_used


    my_engine = Engine('petrol')
    your_engine = Engine('diesel')

    my_engine.start()
    my_engine.rpm = 1000
    used_fuel = my_engine.run(10)
    print('Run my engine at {} rpm used {} L fuel, remaining {} L'.format(my_engine.rpm,
                                                                        used_fuel,
                                                                        my_engine.fuel_level))
    print(vars(my_engine))
    print('Your engine has {} L left'.format(your_engine.fuel_level))


The output of the above will be:

.. code-block:: none

    Run my engine at 1000 rpm used 1.0000000000000003e-05 L fuel, remaining 99.99999 L
    {'fuel_level': 99.99999, 'state': 'running', 'rpm': 1000, 'fuel_type': 'petrol'}
    Your engine has 100.0 L left


Obviously, functional programming and object oriented programming are way beyond the scope of this workshop and entire
but if you are still interested in learning more let us know and we will run a follow up workshop concentrating
on Python and MicroPython programming.

Modules and Source Code
-----------------------

You can put all of your source for your program in a single file and many small embedded projects will be fine
this way. However you can group all of your variables, functions and classes that relate to each other into a module.

Then you can ``import`` this module into other python files.

Extending the above example, you could create an automobile.py file with

.. code-block:: python

    # These are some useful variables for the module
    MINIMUM_RPM = 100
    DEFAULT_FUEL_TYPE = 'petrol'

    # This is a function in the module
    def turn_off(car):
        car.engine.state = 'off'

    # This module has an engine class which is uses internally to define engines in cars
    class Engine:
        rpm = 0      # set initial rpm
        state = 'off'
        fuel_level = 100.0

        # The constructor
        def __init__(self, fuel=DEFAULT_FUEL_TYPE):
            self.fuel_type = fuel

        def start(self):
            self.state = 'running'
            self.rpm = MINIMUM_RPM

        def run(self, seconds):
            if not self.state == 'running':
                print('Engine is not started!')
                return 0
            fuel_used = 0.000000001 * rpm * seconds       # this is a local variable
            self.fuel_level -= fuel_used
            return fuel_used

    # Our little module has the idea of a car which has an engine and a colour
    # (and hopefully wheels in a future version!)
    class Car:
        engine = Engine()

        def __init__(self, colour):
            self.colour = colour

Then in your main program, main.py

.. code-block:: python

    import automobile

    my_car = automobile.Car('blue')
    your_car = automobile.Car('yellow')

    print('An engine must have {} minimum rpm'.format(automobile.MINIMUM_RPM))
    my_car.engine.start()
    print(your_car.fuel_level)
    used_fuel = my_car.engine.run(10)
    print('I used {}L of {} fuel.'.format(used_fuel, my_car.engine.fuel_type))

This is great but you have to type ``automobile`` whenever you want to use stuff in the module, instead you can:

.. code-block:: python

    # Just import what you need from the module
    from automobile import Car, MINIMUM_RPM, turn_off

    my_car = Car('blue')
    my_car.start()
    my_car.run(10)
    turn_off(my_car)

Exceptions
----------

Often you will wish to break out of a function when something unexpected happens. In the Engine example above,
you cannot run the engine without the engine being started. You could print that you need the engine to run like
we did but another way is to raise an exception.

In other cases, maybe your function expects a number but the programmer accidentally sent a string, in this case
Python will raise a ValueError. Perhaps you are using the requests library to grab a web page, what happens if the script cannot
connect to the web server? It will call an exception.

The calling code then needs to handle the exception with a try: except: (else:/finally:) block.

.. code-block:: python

    class Engine:
        ...
         def run(self, seconds):
             if not self.state == 'running':
                raise Exception('Engine is not running!')
            fuel_used = 0.000000001 * rpm * seconds       # this is a local variable
            self.fuel_level -= fuel_used
            return fuel_used

    my_engine = Engine()
    try:
        my_engine.run(10)
    except Exception as e:
        print(e.message)

This code will print the message, 'Engine is not running!' since we did not call the start() method first. Using a try:
except: block will allow your code to take alternative action and fail gracefully.

There are optional clauses to try: except: which are:

* else: This is put after the except: clause and is run if the try block did NOT cause an exception
* finally: This is put after other blocks and is run regardless of if the try block caused an exception.

(Expect the Unexpected!)

Python Standard Library
-----------------------

Up to now we have purely described the language of Python, and that is all the language is. However, you cannot do
much with the language as it is - how do you open and read/write files, or make network connections?

The Python standard library provides much of this useful functionality and is documented
here: https://docs.python.org/3/library/index.html

Here is a list of common uses:

* `Regular expressions`_ - for text searching and replacing
* `Dates and Time`_
* `os.path`_ - For looking for paths in the filesystem
* socket_ - Creating TCP/IP Connections
* urllib_ - Handling URLs
* sqlite3_ - File based database
* json_ - Popular way to transfer data between systems
* threading_ - Thread Based Parallel tasks
* multiprocessing_ - Process Based Parallel tasks
* subprocess_ - Executing other programs from Python
* binascii_ - Converting between binary and ASCII

.. _`Regular expressions`: https://docs.python.org/3/library/re.html
.. _`Dates and Time`: https://docs.python.org/3/library/datetime.html
.. _`os.path`: https://docs.python.org/3/library/os.path.html
.. _socket: https://docs.python.org/3/library/socket.html
.. _urllib: https://docs.python.org/3/library/urllib.html
.. _sqlite3: https://docs.python.org/3/library/sqlite3.html
.. _json: https://docs.python.org/3/library/json.html
.. _threading: https://docs.python.org/3/library/threading.html
.. _multiprocessing: https://docs.python.org/3/library/multiprocessing.html
.. _subprocess: https://docs.python.org/3/library/subprocess.html
.. _binascii: https://docs.python.org/3/library/binascii.html

The Python Ecosystem - PyPi and Pip
-----------------------------------

If something is not in the standard library you can usually find it has already been done and you can install
additional modules using the ``pip`` command. To find these modules, head to https://pypi.python.org/pypi or use
``pip search <SOMETHING>`` to search for the functionality you are looking for.

For example, if you are developing a web application, you might choose *Flask* or *Django* as web frameworks.
If you are looking to try out asyncronous programming you might search for gevent or twisted. Scientific
and data science applications commonly use *numpy, scipy and pandas*.

From here you need to jump on the Internet and do some more learning.
* Learn Python The Hard Way: https://learnpythonthehardway.org/
* Obey the Testing Goat!: https://www.obeythetestinggoat.com/

I am sure I have missed out some very excellent parts of the Python language, but this is just a brain dump of the
basics to get you started and perhaps a cheat sheet you can use to bootstrap your Python learning.

Other Hints and Tips
--------------------

* On your PC, install Python 3 and use it to practice. This even works for Windows.
* The python command on OSX and most Linux refers to the older Python 2.7. You need to install Python3
* On PCs, python scripts usually have ``#!/usr/bin/env python3`` at the top. This Hash-Bang means that the following
    program is a script that needs to be executed with python3.
* On windows, this might be something like ``#!C:\\Python3.5\\bin\python.exe``
* Install IPython and the Jupyter Notebook to practice your python. IPython is a *much* prettier REPL prompt and
    Jupyter notebook means you can keep your notes along with your code.
* Use a code editor or IDE such as PyCharm or Visual Studio code (with Python plugin) or Notepad++ (Windows)
* To learn more about Python, ask us to put on a Python workshop!


.. rubric:: Footnotes

.. [IEEE2017] http://spectrum.ieee.org/computing/software/the-2017-top-programming-languages
.. [#f1] These course notes and slides were generated with Python using Jupyter Notebook
   and Sphinx packages.
.. [#f2] The only Python that matters is Python3. In Python2 is End-Of-Life and not recommended for new stuff.
