.. Hobart Makers LoRa IoT Workshop Notes master file, created by

Hobart Makers LoRa and IoT Workshop
===================================

Contents
--------

.. toctree::
   :maxdepth: 2

   intro
   iot
   wireless
   lora
   python
   micropython
   sensors
   lopyboard
   ttn

Resources
=========

* `Gitlab Repository`_ (code, slides, notes):
  https://gitlab.com/faulteh/LoRa-IoT-Workshop
* `Workshop Notes PDF`_
* `Slide Deck`_

.. _`Gitlab Repository`: https://gitlab.com/faulteh/LoRa-IoT-Workshop
.. _`Workshop Notes PDF`: _static/LoRaIoTWorkshop.pdf
.. _`Slide Deck`: _static/slides/LoRa-IoT-Workshop.html

Code Examples
=============

Can be found at https://gitlab.com/faulteh/LoRa-IoT-Workshop/tree/master/code

0. `WiFi Station`_
1. `TTN With Config`_
2. `Temperature Sensor`_
3. `Ultrasonic Sensor`_
4. `Soil Moisture Sensor`_

.. _`WiFi Station`: https://gitlab.com/faulteh/LoRa-IoT-Workshop/tree/master/code/Code0-WiFiStation
.. _`TTN With Config`: https://gitlab.com/faulteh/LoRa-IoT-Workshop/tree/master/code/Code1-TTN-With-Config
.. _`Temperature Sensor`: https://gitlab.com/faulteh/LoRa-IoT-Workshop/tree/master/code/Code2-TemperatureSensor
.. _`Ultrasonic Sensor`: https://gitlab.com/faulteh/LoRa-IoT-Workshop/tree/master/code/Code3-UltrasonicSensor
.. _`Soil Moisture Sensor`: https://gitlab.com/faulteh/LoRa-IoT-Workshop/tree/master/code/Code4-SoilMoistureSensor

About Hobart Makers
===================

We are a group of people that love making things. We are based in Hobart, 
Tasmania and we welcome people of any age, gender, skill level and from 
any background.

Our goal is to create a large and supportive maker community. We host 
regular meetups, demos and workshops to make this happen.

We want to promote and encourage people to make (or repair/repurpose) 
things. Most of the founders have a background in software and electronics, 
but we want to promote all crafts and skills that are interesting to our members.

Come and meet us and you’ll get the idea! 

* https://hobartmakers.com
