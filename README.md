# LoRa IoT Workshop

A workshop to introduct LoRa/LoRaWAN and IoT networks using TheThingsNetwork
and MicroPython based dev boards.

This workshop was first run in September 2017 by Hobart Makers Inc as part
of National Science Week.

## Development Board

This workshop uses the LoPy development board from PyCom: https://www.pycom.io/product/lopy/

## Workshop Materials (what is in this repo)

The directories in this repo are fairly self-explanatory:

* notes/ - Static site for workshop information and notes, generated with Sphinx using
  ```make html``` and hosted on Gitlab Pages. PDF version with ```make latexpdf```
* slides/ - Workshop slides and trainer notes, sourced using Jupyter
  notebook and turned into a Reveal.JS slide deck using nbconvert
* code/ - Example code for the workshop

