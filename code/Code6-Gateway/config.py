""" LoPy LoRaWAN Nano Gateway configuration options """

GATEWAY_ID = '240ac4ffff008970'

SERVER = 'bridge.asia-se.thethings.network'
PORT = 1700

NTP = "pool.ntp.org"
NTP_PERIOD_S = 3600

WIFI_SSID = 'HobartMakers'
WIFI_PASS = 'makeallthethings'

LORA_FREQUENCY = 917000000
LORA_DR = "SF7BW125"   # DR_5
