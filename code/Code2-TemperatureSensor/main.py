# main.py - main script
# Demonstrates the temperature sensor using the DTH (sic) class

from machine import Pin
from dth import DTH
import time

dht_pin = Pin('G10', mode=Pin.OPEN_DRAIN)

sensor = DTH(dht_pin)
time.sleep(2)

while True:
    read_values = sensor.read()
    print('Temperature: {} Humidity: {}'.format(read_values.temperature, read_values.humidity))
    time.sleep(5)
