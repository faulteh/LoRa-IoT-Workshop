import os
import machine
from network import WLAN

# Replicates the REPL on both telnet and serial
uart = machine.UART(0, 115200)
os.dupterm(uart)

wlan = WLAN(mode=WLAN.STA)

my_nets = {
    'YOURSSID': 'wifipassword',
    'HobartMakers': 'makeallthethings',
}

found_net = None
found_sec = None
nets = wlan.scan()
for net in nets:
    if net.ssid in my_nets.keys():
        found_net = net.ssid
        found_sec = net.sec
        break

print('Connecting to %s' % found_net)
wlan.connect(found_net, auth=(found_sec, my_nets[found_net]), timeout=5000)
while not wlan.isconnected():
    machine.idle()
    print('Network connected');
    break
