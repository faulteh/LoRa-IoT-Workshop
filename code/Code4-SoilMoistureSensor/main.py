# main.py - main script
# Demonstrates the soil moisture sensor

import machine
import time


adc = machine.ADC()
soil = adc.channel(pin='P20')

while True:
    moist = soil()
    print('Moisture reading {}'.format(moist/4095*100))
    time.sleep(1)
