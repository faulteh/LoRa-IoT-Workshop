# main.py - main script
# Demonstrates the temperature sensor using the DTH (sic) class
from machine import Pin, ADC
from network import LoRa
import socket, binascii, struct
import time
import json

from dth import DTH
from ultrasonic import pulse, handle_echo

from config import settings

###############
# Setup LoRaWAN
###############

# Initialize LoRa in LORAWAN mode.
lora = LoRa(mode=LoRa.LORAWAN)

time.sleep(2)

#Setup the single channel for connection to the gateway at it's known frequency
for channel in range(0, 72):
    lora.remove_channel(channel)
for index in range(8, 15):
    lora.add_channel(index,  frequency=settings['ttn']['frequency'],  dr_min=0,  dr_max=settings['ttn']['data_rate'])

# create an ABP authentication params
dev_addr = struct.unpack(">l", binascii.unhexlify(settings['ttn']['device_addr']))[0]
nwk_swkey = binascii.unhexlify(settings['ttn']['network_key'])
app_swkey = binascii.unhexlify(settings['ttn']['app_key'])

# join a network using ABP (Activation By Personalization)
lora.join(activation=LoRa.ABP, auth=(dev_addr, nwk_swkey, app_swkey))

while not lora.has_joined():
    print('Trying...')
    time.sleep(3)

print ('Joined')

# create a LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# set the LoRaWAN data rate
s.setsockopt(socket.SOL_LORA, socket.SO_DR, settings['ttn']['data_rate'])

###################
# Setup the Sensors
###################

en_dht11 = settings['dht11']['enabled']
en_soil = settings['soil']['enabled']
en_sonic = settings['ultrasonic']['enabled']

if en_dht11:
    dht_pin = Pin(settings['dht11']['pin'], mode=Pin.OPEN_DRAIN)
    dht11_sensor = DTH(dht_pin)
if en_soil:
    adc = ADC()
    soil_adc = adc.channel(pin=settings['soil']['pin'])
if en_sonic:
    trigger_pin = Pin(settings['ultrasonic']['trigger_pin'], mode=Pin.OUT)
    trigger_pin.value(0)
    echo_pin = Pin(settings['ultrasonic']['echo_pin'], mode=Pin.IN)
    echo_pin.callback(Pin.IRQ_RISING | Pin.IRQ_FALLING, handler=handle_echo)

time.sleep(2)       # Let the DHT11 bus settle

def read_sensors():
    global en_dht11, en_soil, en_sonic, dht11_sensor, soil_adc
    reading = {}
    if en_dht11:
        d_reading = dht11_sensor.read()
        reading['temp'] = d_reading.temperature
        reading['hum'] = d_reading.humidity
    if en_soil:
        reading['soil'] = soil_adc() / 4095 * 100
    if en_sonic:
        reading['dist'] = pulse(trigger_pin)
    return reading

while True:
    latest = read_sensors()
#
    # Send LoRa Packet
    s.setblocking(True)
    for item in latest:
        print('sending {}={}'.format(item, latest[item]))
        s.send('{}={}'.format(item, latest[item]).encode('utf-8'))
    s.setblocking(False)

    # Receive some bytes
    data = s.recv(64)
    print('Received: {}'.format(data.decode('utf-8')))

    print(json.dumps(latest))
    time.sleep(settings['sample_period'])
