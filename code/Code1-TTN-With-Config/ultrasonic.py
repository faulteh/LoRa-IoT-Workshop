# main.py - main script
# Demonstrates the ultrasonic sensor

from machine import Pin, Timer
import time

end_tick = 0
start_tick = 0
timeout = False

global trigger_pin
global echo_pin

def handle_echo(pin):
    '''
    Sets the start and stop ticks when the echo pin detects it is activated
    :param pin:
    '''
    tick = time.ticks_us()
    if pin.value() == 0:
        global end_tick, timeout
        end_tick = tick
        timeout = True
    else:
        global start_tick
        start_tick = tick

def handle_alarm(arg):
    '''
    Set the timeout so pulse stops waiting
    '''
    global timeout
    timeout = True

def pulse(tpin):
    """
    Sends the ultrasonic pulse and waits up to 1sec for the result
    :return: None (failed) or distance in cm
    """
    distance = None
    global timeout, start_tick, end_tick
    end_tick = 0
    start_tick = 0
    timeout = False
    tpin.value(0)
    time.sleep_ms(2)
    Timer.Alarm(handle_alarm, ms=1000, periodic=False)
    tpin.value(1)
    time.sleep_us(10)
    tpin.value(0)

    while not timeout:
        time.sleep_us(1)

    if start_tick != 0 and end_tick != 0:
        diff = end_tick - start_tick
        distance = diff / 29 / 2;
    return distance

