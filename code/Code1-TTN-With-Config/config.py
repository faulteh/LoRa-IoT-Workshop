# Settings file for LoRa end device with some sensors on it

settings = {
    'dht11': {
        'enabled': True,
        'pin': 'G10',
    },
    'soil': {
        'enabled': True,
        'pin': 'P20',
    },
    'ultrasonic': {
        'enabled': True,
        'echo_pin': 'P11',
        'trigger_pin': 'P12',
    },
    'ttn': {
        'device_addr': '26041AD7',
        'network_key': 'D0A0CD278BA7EB46279B69698EA36279',
        'app_key': '11FE9F985F331AC74DB5B4421630C280',
        'frequency': 917000000,
        'data_rate': 3,
    },
    'sample_period': 30,
}
